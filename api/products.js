const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");
const multer = require('multer');

const Product = require("../model/product");
const auth = require("../middleware/auth");

const app = express();

app.use(express.json({ limit: "50mb" }));

app.use(bodyParser.urlencoded(
  { extended:true }
))

app.set("view engine","ejs");

app.post("/addProduct", auth, async (req, res) => {
  try {
    // Get product input
    const { name, model, variant, year, description, quantity, Categories, subCategories, color, size, weight, price  } = req.body;

    // Validate product input
    if (!(name && model && variant && year && quantity && Categories && price)) {
      res.status(400).send("All mandatory input is required {name, model, variant, year, quantity, Categories, price}");
    }

    // Create product in our database
    const product = await Product.create({
      name,
      model, 
      variant, 
      year, 
      description, 
      quantity, 
      Categories, 
      subCategories, 
      color, 
      size, 
      weight, 
      price
      });
    
    res.status(201).json(product);
  } catch (err) {
    console.log(err);
  }
});


//get products 
app.get("/product", auth, (req, res) => {

  try{

    const {name, model } = req.body;

    //all products
    Product.find({}, (err,allProducts) => { 
    
    //selected product by model or name
    Product.find({ $or:[{ name: name }, { model: model }] }, (err,selectProduct) => {
    
    if (!(name || model)){
      return res.status(200).json(allProducts)
    }
    else if (name || model){
      return res.status(200).json(selectProduct)
    }
    else{
      return res.status(204).send("!product not found...", err)
    }
  });
  });
  } catch (err){
    res.json(err);
  }
});


//update product Quantity or Price 
app.put("/updateProduct", auth, (req, res) => {

  try{
    const {product_id, price, quantity } = req.body;

    // Validate product input
    if (!(product_id)) {
      res.status(400).send("! please provide product ID... ");
    }

    if (!(price || quantity)) {
      res.status(400).send("! please provide at least one of them price or quantity... ");
    }

    //update product
    Product.findOne({ product_id: product_id }, (err, updateProduct) => {

    if (!(updateProduct)){
      res.status(400).send("!Oops product not found...")
    }
    else{
      //product update
      Product.updateOne( {product_id: product_id}, {$set: { $or: [{ price: price },{ quantity: quantity }] } }, (err, productUpdate) => {
        res.status(200).json(productUpdate)
      })
    }
  });
  } catch (err){
    res.json(err)
  }  
});


//delete product form DB
app.delete("/deleteProduct", auth, (req, res) => {

  try{
    const {product_id} = req.body;

    // Validate product input
    if (!(product_id)) {
      res.status(400).send("! please provide product ID... ");
    }
    Product.findOne({ product_id: product_id }, (err, deleteProduct) =>{
      console.log(deleteProduct)

    if (!(deleteProduct) || null || deleteProduct.length === 0 ){
      res.status(400).send("!Oops product not found...")
    }
    else{
      //delete inventory product
      Product.deleteOne({product_id: product_id }, (err, result) => {
        res.status(200).json(result)
    });
    }
  });
  } catch (err){
    res.json(err)
  }  
});


//product image upload
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
     cb(null, 'uploads'); //location uploads
  },
  filename: function (req, file, cb) {
     cb(null, Date.now() + '-' + file.originalname); //filename
     }
});


var upload = multer({ storage: storage });


app.post('/imageUpload', auth, upload.single('image'),(req, res) => {

  try{
    const { product_id } = req.body;

    if (!(product_id)){
      res.status(201).send("!Oops you not provide product id... ")
    }

    Product.findOne( { product_id: product_id }, (err, result) =>{
      
    if (!(result)){
      res.status(201).send("!Oops product not found... ")
    }
          
    var fileName = req.file.path
    
    var img = fs.readFileSync(req.file.path);
    var encode_img = img.toString('base64');
    var final_img = {
        contentType: req.file.mimetype,
        image: new Buffer(encode_img,'base64')
    }
    
    Product.findOneAndUpdate(
      { product_id : product_id },
      { $push: { img : {'data' : final_img.image, 'contentType': final_img.contentType, 'path': fileName} } }, (err, result) => {
        
      res.send(200).send("Image upload successfully...")
    })
  });
} catch(err){
  res.json(err)
}
});


module.exports = app;
