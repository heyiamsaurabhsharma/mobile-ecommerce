const express = require("express");

const Cart = require("../model/cart");
const auth = require("../middleware/auth");

const app = express();

app.use(express.json({ limit: "50mb" }));

app.post("/addToCart", auth, async (req, res) => {
  try {
    // Get product input
    const { user_id, product_id, name, model, variant, year, color,size, weight, price, purchase_quantity } = req.body;

    // Validate product input
    if (!(user_id && product_id && name && model && variant && year && price && purchase_quantity)) {
      res.status(400).send("All mandatory input is required {user_id, product_id, name, model, variant, year, purchase_quantity, Categories, price}");
    }

    // Create product in our database
    const product = await Cart.create({
      user_id,
      product: [{        
        product_id,
        name,
        model, 
        variant, 
        year,
        color, 
        size, 
        weight, 
        price,
        purchase_quantity
    }],
      });
    
    res.status(201).json(product);
  } catch (err) {
    res.json(err);
  }
});


//get products 
app.get("/cart", auth, (req, res) => {

  try{
    const {user_id, name, order_id } = req.body;

    // validate cart input
    if (!(user_id || name || order_id)) {
      res.status(400).send("provide any of them {user id, product name, or order id}");
    }
    //all products in cart
    Cart.find({ user_id: user_id }, (err, allProducts) => {

    if (!(allProducts)){
      res.status(400).send("! Oops No Product In The Cart ...")
    }

    //selected product in cart
    Cart.findOne({ $or:[{ name: name }, { order_id: order_id }] }, (err, selectProduct) => {

    if (!(name || order_id)){
      return res.status(200).json(allProducts)
    }
    else if (name || order_id){
      return res.status(200).json(selectProduct)
    }
    else{
      return res.status(204).send("!product not found...")
    }
    });
    });
  } catch (err){
    res.json(err)
  }
 });


//update product Quantity
app.put("/updateCart", auth, (req, res) => {

  try{

    const {order_id, purchase_quantity } = req.body;

    // Validate product input
    if (!(order_id)) {
      res.status(400).send("! please provide Order ID... ");
    }

    if (!(purchase_quantity)) {
      res.status(400).send("! please provide updated quantity... ");
    }

    if (!(purchase_quantity >= 1)){
      res.status(203).send("purchase quantity is not less then 1...")
      
    }

    //update product
    Cart.findOne({"product" : { $elemMatch : {order_id : order_id} }}, (err, updateCart) =>{

    if (!(updateCart) || updateCart.length === 0 ){
      res.status(400).send("!Oops product not found...")
    }
    else{
      //product update
      var query = { order_id : order_id };
      var data = { 
        $set: {
          'product.$[].purchase_quantity' : purchase_quantity 
        }
      }

      Cart.updateOne(query , data, (err, cartUpdate) =>{
      res.status(200).json(cartUpdate)
      
    });
    }
  });
} catch (err){
  res.json(err)
}
});  


//delete product form DB
app.delete("/deleteCartProduct", auth, (req, res) => {

  try{

    const {order_id} = req.body;

    
    // Validate product input
    if (!(order_id)) {
      res.status(400).send("! please provide Order ID... ");
    }
    Cart.findOne( {"product" : { $elemMatch : {order_id : order_id} }} , (err, deleteCartProduct) =>{

      if (!(deleteCartProduct) || null || deleteCartProduct.length === 0 ){
        res.status(400).send("!Oops Order not found...")
      }
      else{
        //delete cart product
        Cart.deleteOne( {"product" : { $elemMatch : {order_id : order_id} }}, (err, result) => {
          res.status(200).json(result)
      });
    }
  });
} catch (err){
  res.json(err)
}
});

module.exports = app;
