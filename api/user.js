const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require("../model/user");


const app = express();

//register
app.post("/register", async (req, res) => {
  try {
    // Get user input
    const { first_name, last_name, email, phone, password } = req.body;

    // Validate user input
    if (!(email && phone && password && first_name && last_name)) {
      res.status(400).send("All input is required {first name, last name, email, phone, password}");
    }

    // check if user already exist
    // Validate if user exist in our database
    const oldUserEmail = await User.findOne({ email });

    if (oldUserEmail) {
      return res.status(409).send("User Email Already Exist. Please Login");
    }

    const oldUserPhone = await User.findOne({ phone });

    if (oldUserPhone) {
      return res.status(409).send("User Phone Already Exist. Please Login");
    }

    //Encrypt user password
    const encryptedPassword = await bcrypt.hash(password, 10);

    // Create user in our database
    const user = await User.create({
      first_name,
      last_name,
      email: email.toLowerCase(), 
      phone,  
      password: encryptedPassword,
    });

    // Create token
    const token = jwt.sign(
      { user_id: user._id, email, phone },
      process.env.TOKEN_KEY,
      {
        expiresIn: "2h",
      }
    );
    // save user token
    user.token = token;

    // return new user
    res.status(201).json(user);
  } catch (err) {
    console.log(err);
  }
});


//login
app.post("/login", async (req, res) => {
  try {
    // Get user input
    const { email, password } = req.body;

    // Validate user input
    if (!(email && password)) {
      res.status(400).send("All input is required {user id and password}");
    }
    // Validate if user exist in our database
    const user = await User.findOne({email: email });

    if (user && (await bcrypt.compare(password, user.password))) {
      // Create token
      const token = jwt.sign(
        { user_id: user._id, email },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }
      );

      // save user token
      user.token = token;

      // user
      res.status(200).json(user);
    }
    res.status(400).send("Invalid Credentials");
  } catch (err) {
    res.json(err);
  }
});


module.exports = app;
