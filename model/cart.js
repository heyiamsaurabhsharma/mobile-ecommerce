const { Schema, model } = require("mongoose");

const cartSchema = new Schema({
    user_id: { type: String },
    
    product: [{
        order_id: {type: Schema.Types.ObjectId,
            index: true,
            required: true,
            unique: true,
            auto: true},
        product_id: { type: String, required: true },
        name: { type: String, required: true },
        model: { type: String, required: true },
        variant: { type: String, required: true },
        year: {type: Number, required: true },
        color: { type: String, default: null },
        size: { type: String, default: null},
        weight: { type: String, default: null},
        price: { type: Number, required: true },
        purchase_quantity: {type: Number, required: true},
        time : { type: Number, default: (new Date()).getTime() }
    }],
    
});

module.exports = model("cart", cartSchema);
