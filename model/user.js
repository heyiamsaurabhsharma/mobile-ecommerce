const { Schema, model } = require("mongoose");

const userSchema = new Schema({
  first_name: { type: String, default: null },
  last_name: { type: String, default: null },
  email: { type: String, unique: true, required: true },
  phone: { type: Number, unique: true, required: true },
  password: { type: String, required: true },
  token: { type: String },
  time : { type: Number, default: (new Date()).getTime() }
});

module.exports = model("user", userSchema);
