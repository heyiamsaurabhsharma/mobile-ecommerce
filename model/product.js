const { Schema, model } = require("mongoose");

const productSchema = new Schema({
    product_id: {type: Schema.Types.ObjectId,
        required: true,
        auto: true,
        unique: true},
 
    name: { type: String, required: true },
    model: { type: String, required: true },
    variant: { type: String, required: true },
    year: {type: Number, required: true },
    description: { type: String, default: null },
    quantity: {type: Number, required: true },
    Categories: { type: String, required: true },
    subCategories: { type: String, default: null },
    color: { type: String, default: null },
    size: { type: String, default: null},
    weight: { type: String, default: null},
    price: { type: Number, required: true },

    img:
    {
        data: Buffer,
        contentType: String,
        path: String
    },

    time : { type: Number, default: (new Date()).getTime() }
});

module.exports = model("product", productSchema);
