
require("dotenv").config();
require("./config/database").connect();

const express = require("express");
const bodyParser = require('body-parser');

const app = express();

const user = require("./api/user");
const product = require("./api/products");
const cart = require("./api/cart");

const { API_PORT } = process.env;
const port = process.env.PORT || API_PORT;

app.use(express.json({ limit: "50mb" }));
app.use(bodyParser.json());

app.use(user);
app.use(product);
app.use(cart);

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
